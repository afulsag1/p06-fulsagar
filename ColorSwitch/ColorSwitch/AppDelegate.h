//
//  AppDelegate.h
//  ColorSwitch
//
//  Created by Abhijit Fulsagar on 4/13/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    AVAudioPlayer *audio;
}
@property (strong, nonatomic) UIWindow *window;


@end

