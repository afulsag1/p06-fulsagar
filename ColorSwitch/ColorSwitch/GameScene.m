//
//  GameScene.m
//  ColorSwitch
//
//  Created by Abhijit Fulsagar on 4/13/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene {
    CAShapeLayer *circleLayer;
    int angle[5];
}

- (void)didMoveToView:(SKView *)view {

    angle[0]=0;
    angle[1]=90;
    angle[2]=180;
    angle[3]=270;
    angle[4]=0;
    NSLog(@"hello");
    // Do any additional setup after loading the view, typically from a nib
    [self obstacle];
    [self player];
}
-(void)obstacle
{
    for(int i=0;i<5;i++)
    {
        circleLayer = [CAShapeLayer layer];
        [circleLayer setPath:[[UIBezierPath bezierPathWithArcCenter:CGPointMake(150, 150)
                                                             radius:50
                                                         startAngle:((angle[i]*3.14)/180)
                                                           endAngle:((3.14*angle[i+1])/180)
                                                          clockwise:YES] CGPath ]  ];
        if(i==0)
            [circleLayer setStrokeColor:[[UIColor purpleColor] CGColor]];
        else if(i==1)
            [circleLayer setStrokeColor:[[UIColor redColor] CGColor]];
        else if(i==2)
            [circleLayer setStrokeColor:[[UIColor yellowColor] CGColor]];
        else
            [circleLayer setStrokeColor:[[UIColor greenColor] CGColor]];
        
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        [circleLayer setLineWidth:7.5];
        [[self.view layer] addSublayer:circleLayer];
        //circleLayer.transform=CGAffineTransformMakeRotation(M_PI);
    }
}
-(void)player
{
    
}

@end
