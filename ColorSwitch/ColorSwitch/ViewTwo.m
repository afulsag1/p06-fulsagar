//
//  ViewTwo.m
//  ColorSwitch
//
//  Created by Abhijit Fulsagar on 4/17/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>

@interface ViewTwo : UIViewController
{
    CAShapeLayer *circleLayer[4];
    CAShapeLayer *shapeLayer[4];
    CAShapeLayer *player;
    int angle[5];
    int r;
    int startpoint[5][2];
    UIButton *button;
    UITapGestureRecognizer *taprecognizer;
    CGPoint current;
    CGRect screenRect;
    CGSize screenSize ;
    CGFloat screenWidth;
    CGFloat screenHeight;
    NSTimer *timer,*timer1,*timer2;
    UIAlertAction *alert;
    int tag[4];
    NSString *textscore;
}
@property (weak, nonatomic) IBOutlet UILabel *labelscore;
@property (weak, nonatomic) IBOutlet UILabel *labelhighscore;
@end

@implementation ViewTwo
- (void)viewDidLoad {
    [super viewDidLoad];
    _labelscore.text=@"0";
    screenRect = [[UIScreen mainScreen] bounds];
    screenSize = screenRect.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    // NSLog(@"screenheight:%f",screenHeight);
    angle[0]=0;
    angle[1]=90;
    angle[2]=180;
    angle[3]=270;
    angle[4]=0;
    startpoint[0][0]=100;
    startpoint[0][1]=150;
    startpoint[1][0]=100;
    startpoint[1][1]=250;
    startpoint[2][0]=200;
    startpoint[2][1]=250;
    startpoint[3][0]=200;
    startpoint[3][1]=150;
    startpoint[4][0]=100;
    startpoint[4][1]=150;
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *value=[defaults objectForKey:@"value"];
    _labelhighscore.text=value;

    
    //NSLog(@"hello");
    taprecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touch:)];
    taprecognizer.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:taprecognizer];
    
    
    // Do any additional setup after loading the view, typically from a nib
    
    [self player];
    [self createobstacle];
   timer=[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(movingdown:) userInfo:NULL repeats:YES ];
    
    timer1=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(collision:) userInfo:NULL repeats:YES ];
    
    timer2=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(obstacledown:) userInfo:NULL repeats:YES ];
    
    
    
}
-(void)createobstacle
{
    r=arc4random_uniform(2);
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    animation.fromValue=[NSNumber numberWithFloat:0.0f];;
    animation.byValue = @(2*M_PI);
    animation.duration = 10.0f;
    animation.speed=2;
    animation.repeatCount = INFINITY;
    animation.removedOnCompletion = NO;
    
    

    if(r==0)
    {
        for(int i=0;i<4;i++)
        {
            circleLayer[i] = [CAShapeLayer layer];
            [circleLayer[i] setFrame:CGRectMake(180, 180, 370, 370)];
            [ circleLayer[i] setPath:[[UIBezierPath bezierPathWithArcCenter:CGPointMake(180, 180)
                                                                     radius:65
                                                                 startAngle:((angle[i]*3.14)/180)
                                                                   endAngle:((3.14*angle[i+1])/180)
                                                                  clockwise:YES]CGPath ]];
            //circleLayer.bounds = CGPathGetBoundingBox(circle.CGPath);
            [circleLayer[i] setPosition:CGPointMake(175, 180)];
           
            if(i==0)
            {
                [circleLayer[i] setStrokeColor:[[UIColor purpleColor] CGColor]];
                shapeLayer[i].strokeColor = [[UIColor purpleColor] CGColor];
            }
            else if(i==1)
            {
                [circleLayer[i] setStrokeColor:[[UIColor redColor] CGColor]];
                shapeLayer[i].strokeColor = [[UIColor redColor] CGColor];
            }
            else if(i==2)
            {
                [circleLayer[i] setStrokeColor:[[UIColor yellowColor] CGColor]];
                shapeLayer[i].strokeColor = [[UIColor yellowColor] CGColor];
            }
            else
            {
                [circleLayer[i] setStrokeColor:[[UIColor greenColor] CGColor]];
                shapeLayer[i].strokeColor = [[UIColor greenColor] CGColor];
            }
            
            [circleLayer[i] setFillColor:[[UIColor clearColor] CGColor]];
            [circleLayer[i] setLineWidth:12];
            
            [[self.view layer] addSublayer:circleLayer[i]];
            [circleLayer[i] addAnimation:animation forKey:@"SpinAnimation"];

        }
    }
    else if(r==1)
    {
        for(int i=0;i<4;i++)
        {
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointMake(startpoint[i][0], startpoint[i][1])];
            [path addLineToPoint:CGPointMake(startpoint[i+1][0], startpoint[i+1][1])];
            shapeLayer[i] = [CAShapeLayer layer];
            shapeLayer[i].path = [path CGPath];
            [shapeLayer[i] setFrame:CGRectMake(30, 30, 300, 300)];
            
            shapeLayer[i].lineWidth = 12.0;
            if(i==0)
            {
                shapeLayer[i].strokeColor = [[UIColor purpleColor] CGColor];
            }
            else if(i==1)
            {
                shapeLayer[i].strokeColor = [[UIColor redColor] CGColor];
            }
            else if(i==2)
            {
                shapeLayer[i].strokeColor = [[UIColor yellowColor] CGColor];
            }
            else
            {
                shapeLayer[i].strokeColor = [[UIColor greenColor] CGColor];
            }

            shapeLayer[i].fillColor = [[UIColor clearColor] CGColor];
            [self.view.layer addSublayer:shapeLayer[i]];
            [shapeLayer[i] addAnimation:animation forKey:@"SpinAnimation"];
        }
    }
    
}

-(void)player
{
    player = [CAShapeLayer layer];
    [player setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(85, 450, 20, 20)] CGPath]];
    [player setPosition:CGPointMake(85, 150)];
    [player setStrokeColor:[[UIColor redColor] CGColor]];
    [player setFillColor:[[UIColor redColor] CGColor]];
    [[self.view layer] addSublayer:player];
    // NSLog(@"In player function player y:%f",player.position.y);
   
}
-(void)updatehighscore
{
    int value=[_labelhighscore.text integerValue];
    int value1=[_labelscore.text integerValue];
    if(value<value1)
    {
        _labelhighscore.text=[@(value1) stringValue];
        [self savehighscore];
    }
}
-(void)savehighscore
{
    [_labelhighscore resignFirstResponder];
    NSString *value=_labelhighscore.text;
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"value"];
    [defaults synchronize];
    NSLog(@"data saved");
}
-(void)obstacledown:timer
{
    if(player.position.y<=-430)
    {
        
        [UIView animateWithDuration:10
                         animations:^
         {
             [player setPosition:CGPointMake(player.frame.origin.x, 150)];
             for(int i=0;i<4;i++)
             {
                 [circleLayer[i] setPosition:CGPointMake(175, screenHeight+200)];
                 [shapeLayer[i] setPosition:CGPointMake(175, screenHeight+200)];
             }
         }//animation ends here
         ];//uiview ends here
        textscore=_labelscore.text;
        int score=[textscore integerValue];
        score=score+5;
        textscore=[NSString stringWithFormat:@"%d",score];
        _labelscore.text=textscore;
        [self updatehighscore];
        [self createobstacle];
    }
}
-(void)collision:timer
{
    for(int i=0;i<4;i++)
    {
        
        NSLog(@"circle x:%f    y:%f",circleLayer[i].position.x, circleLayer[i].position.y);

    }
    
    if(player.position.y<=(-220) && player.position.y>=(-240) )
    {
        for(int i=0;i<4;i++)
        {
            if(circleLayer[i].strokeColor==player.strokeColor)
            {
                NSLog(@"pass Color:%s",player.strokeColor);
                [timer invalidate];
                [timer1 invalidate];
            }
        }
    }
}

-(void)endgame
{
    if(player.frame.origin.y>=200)
    {
        [timer invalidate];
        alert = [[UIAlertView alloc] initWithTitle:@"END GAME"
                                           message:@"You Lose"
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
        //[alert show];
    }
}

-(void)movingdown:timer
{
    current=player.frame.origin;
    current.y=200;
    [UIView animateWithDuration:1.5
                     animations:^
     {
         [player setPosition:CGPointMake(player.frame.origin.x,player.frame.origin.y+30)];
     }//animation ends here
                     completion:^(BOOL finished)
     {
     }// completion ends here
     ];//uiview ends here
    // [self endgame];
}

-(void)touch:(UITapGestureRecognizer *) sender
{
    // SKSpriteNode *play=[SKSpriteNode spriteNodeWithColor:(UIColor *)blueColor size:(CGSize)5];
    // NSLog(@"player y:%f ",player.position.y);
    
    [UIView animateWithDuration:2 // to move player up on touch
                     animations:^
                    {
                            [player setPosition:CGPointMake(player.frame.origin.x, player.frame.origin.y-80)];
                    }//animation ends here
     ];//uiview ends here
   
}

@end
